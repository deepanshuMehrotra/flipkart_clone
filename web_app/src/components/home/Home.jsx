import { useEffect } from 'react'
import NavBar from "./NavBar"
import Banner from "./Banner"
import Slide from "./Slide"
import MidSlide from './MidSlide'
import MidSection from './MidSection'

import { Box, styled } from "@mui/material"
import { getProducts } from "../../redux/actions/productsAction"
import { useDispatch, useSelector } from 'react-redux';

const Container = styled(Box)`
padding:10px 10px;
background-color:#f1f3f6
`
const Home = () => {

    const { products } = useSelector(state => state.getProducts)//getProducts is the reducer made in store

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getProducts());// getProducts is the function in the productsAction file
    }, [dispatch]);



    return (
        <>
            <NavBar />
            <Container>
                <Banner />
                <MidSlide products={products} title="Deal of the Day" timer={true} />
                <MidSection />
                <Slide products={products} title="Discounts for you" timer={false} />
                <Slide products={products} title="Suggesting Items" timer={false} />
                <Slide products={products} title="Top Selection" timer={false} />
                <Slide products={products} title="Recommended Items" timer={false} />
                <Slide products={products} title="Trending offers" timer={false} />
                <Slide products={products} title="Season's top picks" timer={false} />
                <Slide products={products} title="Top Deals on Accessories" timer={false} />

            </Container>
        </>
    )
}
export default Home