import { Typography, Box, styled } from "@mui/material"
import { Link } from "react-router-dom"
import Home from "../home/Home"

const Wrapper = styled(Box)`
height:80vh;
width:80%;
background:#fff;
margin:80px 140px;
`
const Container = styled(Box)`
text-align:center;
padding:60px 5px;
`

const LinkToHomePage = styled(Link)`

`

const EmptyCart = () => {
    const imgurl = 'https://rukminim1.flixcart.com/www/800/800/promos/16/05/2019/d438a32e-765a-4d8b-b4a6-520b560971e8.png?q=90'
    return (
        <Wrapper>
            <Container>
                <img src={imgurl} alt='empty cart Image' style={{ width: "35%", marginBottom: '30px', marginTop: '30px' }} />
                <Typography style={{ fontWeight: '600' }}>Your Cart is Empty</Typography>
                <LinkToHomePage to=<Home />>
                    <Typography style={{ fontWeight: '600' }}>Add Items To Your Cart</Typography>
                </LinkToHomePage>
            </Container>
        </Wrapper>

    )
}
export default EmptyCart;