import { Box, styled, Button, ButtonGroup } from '@mui/material';

const StyleButton = styled(Button)`
border-radius:50%;
`

const ButtonGroup1 = () => {
    return (
        <ButtonGroup style={{ marginTop: '30px' }}>
            <StyleButton>-</StyleButton>
            <StyleButton disabled>1</StyleButton>
            <StyleButton>+</StyleButton>
        </ButtonGroup>
    )
}
export default ButtonGroup1;