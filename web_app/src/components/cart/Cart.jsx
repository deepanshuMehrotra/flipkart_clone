import { Typography, Box, Grid, styled, Button } from "@mui/material";
import { useSelector } from "react-redux";

import CartItem from "./CartItem";
import CartPrice from "./CartPrice";
import EmptyCart from "./EmptyCart";
import { payUsingPaytm } from "../../service/api";
import { post } from "../../utils/paytm";

const Container = styled(Grid)(({ theme }) => ({
    padding: '30px 130px',
    [theme.breakpoints.down('md')]: {
        padding: '15px 0px'
    }
}))


const Header = styled(Box)`
padding: 15px 24px;
background: #fff;
`
const ButtonWrapper = styled(Box)`
padding: 16px 22px;
background: #fff;
box-shadow: 0px 0px 10px 0px rgb(0 0 0 / 10 %)
border-top: 1px solid #f0f0f0;
`
const OrderButton = styled(Button)`
display: flex;
margin-left: auto;
border-radius: 1px;
font-weight: 500;
height: 51px;
width: 250px;
color: #fff;
background: #fb641b;
`
const LeftComponent = styled(Grid)(({ theme }) => ({
    paddingRight: 15,
    [theme.breakpoints.down('md')]: {
        marginBottom: 15
    }

}))


const Cart = () => {
    const { cartItems } = useSelector(state => state.cart);

    const buyNow = async () => {
        let response = await payUsingPaytm({ amount: 500, email: 'd7parkzilla@gmail.com' });
        let information = {
            action: 'https://securegw-stage.paytm.in/order/process',
            params: response
        }
        post(information);
    }


    return (
        <>
            {
                cartItems.length ?
                    < Container container>
                        <LeftComponent item lg={9} md={9} sm={12} xs={12}>
                            <Header>
                                <Typography>My Cart ({cartItems.length})</Typography>
                            </Header>
                            {
                                cartItems.map(item => (
                                    <CartItem item={item} />
                                ))
                            }
                            <ButtonWrapper>
                                <OrderButton onClick={() => buyNow()}> PLACE ORDER</OrderButton>

                            </ButtonWrapper>
                        </LeftComponent>
                        <Grid item lg={3} md={3} sm={12} xs={3}>
                            <CartPrice cartItems={cartItems} />
                        </Grid>

                    </Container >
                    : <EmptyCart />
            }
        </>
    )

}
export default Cart;