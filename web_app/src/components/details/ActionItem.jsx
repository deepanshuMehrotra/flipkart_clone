import { Box, Button, styled } from '@mui/material';
import { ShoppingCart as CartIcon, FlashOn as Flash } from '@mui/icons-material';
import { useNavigate } from 'react-router-dom'; //it's a custom hook we have to initialize it below
import { useDispatch } from 'react-redux';
import { addToCart } from '../../redux/actions/cartAction'
import { useState } from 'react';
import { payUsingPaytm } from '../../service/api.js';
import { post } from '../../utils/paytm.js';

const LeftContainer = styled(Box)(({ theme }) => ({
    minWidth: '40%',
    padding: '40px 0 0 80px',
    [theme.breakpoints.down('lg')]: {
        padding: '20px 40px'
    }

}))

const Image = styled('img')({
    padding: '15px'
})

const StyleButton = styled(Button)(({ theme }) => ({
    width: '48%',
    height: 50,
    borderRadius: 2,
    [theme.breakpoints.down('lg')]: {
        width: '46%'
    },
    [theme.breakpoints.down('sm')]: {
        width: '48%'
    }
}))


const ActionItem = ({ product }) => {

    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [quantity, setQuantity] = useState(1);
    const { id } = product;

    const addItemToCart = () => {
        //first it dispatch the product id from the action items and then put it in the redux using 'cartReducer' then navigate on browser
        dispatch(addToCart(id, quantity));
        navigate('/cart');
    }
    const buyNow = async () => {
        let response = await payUsingPaytm({ amount: 100, email: 'd7parkzilla@gmail.com' });
        let information = {
            action: 'https://securegw-stage.paytm.in/order/process',
            params: response
        }
        post(information);
    }
    return (
        <LeftContainer>
            <Box style={{ padding: '15px 20px', border: '1px solid #f0f0f0', width: '90%' }}>
                <Image src={product.detailUrl} alt="product" />
            </Box>
            <StyleButton variant="contained" onClick={() => addItemToCart()} style={{ marginRight: 10, background: '#ff9f00' }}><CartIcon />Add to Cart</StyleButton>
            <StyleButton variant="contained" onClick={() => buyNow()} style={{ background: '#fb541b' }}><Flash />Buy Now</StyleButton>
        </LeftContainer>

    )
}
export default ActionItem