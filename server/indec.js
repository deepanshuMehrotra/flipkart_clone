import express from "express";
import dotenv from 'dotenv';
import cors from 'cors';
import bodyParser from 'body-parser';
import { v4 as uuid } from 'uuid'
import DefaultData from "./default.js";
import Connection from './Database/db.js';
import Router from "./routes/route.js";

const app = express();//  the express

dotenv.config();//env file is never pushed on github and thats why all our password is secure under env

app.use(cors());
app.use(bodyParser.json({ extended: true }))
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', Router);

const PORT = 8000;

const USERNAME = process.env.DB_USERNAME;
const PASSWORD = process.env.DB_PASSWORD;

Connection(USERNAME, PASSWORD);

app.listen(PORT, () => console.log(`Server is running successfully on PORT ${PORT}`));

DefaultData();

export let paytmMerchantkey = process.env.PAYTM_MERCHANT_KEY

export let paytmParams = {};

paytmParams['MID'] = process.env.PAYTM_MID,
    paytmParams['WEBSITE'] = process.env.PAYTM_WEBSITE,
    paytmParams['CHANNEL_ID'] = process.env.PAYTM_CHANNEL_ID,
    paytmParams['INDUSTRY_TYPE_ID'] = process.env.PAYTM_INDUSTRY_TYPE_ID,
    paytmParams['ORDER_ID'] = uuid(),
    paytmParams['CUST_ID'] = process.env.PAYTM_CUST_ID,
    paytmParams['TXN_AMOUNT'] = '100',
    paytmParams['CALLBACK_URL'] = 'http://localhost:8000/callback',
    paytmParams['EMAIL'] = 'd7parkzilla@gmail.com',
    paytmParams['MOBILE'] = '8563933242';

